import avatar from './assets/images/48.jpg'
import style from './App.module.css'
function App() {
  return (
    <div className={style.dcContainer}>
      <div>
        <img src={avatar} alt="img-avatar" className={style.dcAvatar}/>
      </div>
      <div className={style.dcQuote}>
        This is one of the best developer blogs on the planet! I read it,daily to improve my skills.
      </div>
      <div>
        <span className={style.dcName}>
          Tammy stevents
        </span>
        <span>
        &nbsp; * &nbsp;Front End developer
        </span>
      </div>
    </div>
  );
}

export default App;
